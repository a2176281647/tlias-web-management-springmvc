package com.itheima.tliaswebmanagement.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 这是一个封装类，通过&#064;ConfigurationProperties将配置信息注入封装类，在阿里云上传工具类中注入该封装类，并使用封装类封装的配置信息。
 */
@Component
@Data
public class AliOSSProperties {
    //字段名必须与配置文件中的key名称一致
    private String endpoint;
    private String accessKeyId;
    private String secretAccessKey;
    private String bucketName;
}
