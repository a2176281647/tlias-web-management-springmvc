package com.itheima.tliaswebmanagement.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

public class JwtUtils {
    static String signKey = "itheima";

    /**
     * 生成JWT
     *
     * @return JWT令牌
     */
    public static String generateJwt(Map<String, Object> claims) {
        // Jwts.builder()是Java中的一种构造函数，用于创建一个JWT（JSON Web Token）的构建器对象。该构建器对象可以用于构建和签署JWT。
        return Jwts.builder()
                // 接着，signWith()方法用于指定JWT的签名算法和密钥。
                .signWith(SignatureAlgorithm.HS256, signKey)
                // 用于在 JWT（JSON Web Token）构建器中设置声明（claims）。声明是 JWT 中包含的关于特定实体的信息，例如用户 ID、用户名、电子邮件地址等。
                .setClaims(claims)
                // 用于在 JWT（JSON Web Token）构建器中设置过期时间
                .setExpiration(new Date(System.currentTimeMillis() + 3600 * 1000))//有效期为1h
                // 使用 compact() 方法将所有设置组合成一个紧凑的字符串，该字符串可以发送到接收者。
                .compact();
    }

    /**
     * 解析JWT令牌
     *
     * @param jwt JWT令牌
     * @return 解析后的令牌的Payload（有效载荷）部分
     */
    public static Claims parseJwt(String jwt) {
        // 一旦令牌任意部分被篡改或者令牌过期，都会出现异常。
        return Jwts.parser()
                .setSigningKey(signKey)// 匹配生成JWT时的密钥
                // 注意！是parseClaimsJws而不是parseClaimsJwt！
                .parseClaimsJws(jwt)
                // 获取有效载荷
                .getBody();
    }
}
