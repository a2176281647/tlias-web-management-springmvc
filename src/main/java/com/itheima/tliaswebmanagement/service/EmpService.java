package com.itheima.tliaswebmanagement.service;

import com.itheima.tliaswebmanagement.pojo.Emp;
import com.itheima.tliaswebmanagement.pojo.PageBean;

import java.time.LocalDate;
import java.util.List;

public interface EmpService {

    /**
     * 分页查询<br>
     * 由于需求文档要求我们返回的响应参数不仅包含单页所有员工信息,还要包含总记录数等信息
     * 而集合只能存储一种类型的数据,所有必须将这些信息封装成类
     *
     * @param page     页码
     * @param pageSize 每页展示记录数
     * @return List<Emp>
     */
    PageBean pageList(String name, Short gender, LocalDate begin, LocalDate end, Integer page, Integer pageSize);

    /**
     * 根据id批量删除员工
     *
     * @param ids id列表
     */
    void deleteByIds(List<Integer> ids);

    /**
     * 根据部门号删除员工<br>
     * 如果部门被删除而部门下的员工没有被删除,就会破坏数据的完整性和一致性
     *
     * @param deptId 部门号
     */
    void deleteByDeptId(Integer deptId);

    /**
     * 添加员工
     *
     * @param emp Emp
     */
    void insert(Emp emp);

    /**
     * 根据id查询员工<hr>
     * 更新员工分为两步: <br>
     * 1.查询回显<br>
     * 2.提交更新
     *
     * @param id ID
     */
    Emp selectById(Integer id);

    /**
     * 更新员工分为两步:<br>
     * 1.查询回显<br>
     * 2.提交更新
     *
     * @param emp Emp
     */
    void update(Emp emp);

    /**
     * 根据用户名和密码查询
     */
    Emp selectByUsernameAndPassword(Emp emp);
}
