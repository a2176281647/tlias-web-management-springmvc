package com.itheima.tliaswebmanagement.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.tliaswebmanagement.mapper.EmpMapper;
import com.itheima.tliaswebmanagement.pojo.Emp;
import com.itheima.tliaswebmanagement.pojo.PageBean;
import com.itheima.tliaswebmanagement.service.EmpService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service//将当前类交给IOC容器管理,将来会成为IOC容器中的bean
public class EmpServiceImpl implements EmpService {
    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public EmpServiceImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    /**
     * 分页查询<br>
     * 由于需求文档要求我们返回的响应参数不仅包含单页所有员工信息,还要包含总记录数等信息
     * 而集合只能存储一种类型的数据,所有必须将这些信息封装成类
     *
     * @param page     页码
     * @param pageSize 每页展示记录数
     * @return List<Emp>
     */
    @Override
    public PageBean pageList(String name, Short gender, LocalDate begin, LocalDate end, Integer page, Integer pageSize) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);

            // 设置分页参数
            try (Page<Object> ignored = PageHelper.startPage(page, pageSize)) {
                // 执行查询操作,此时获取的empList不是一般的List集合,而是Page类型的集合,Page继承了ArrayList
                List<Emp> empList = empMapper.pageList(name, gender, begin, end, page, pageSize);
                // 父类无法调用子类的特有方法,所以需要将empList强转为Page类型
                Page<Emp> p = (Page<Emp>) empList;
                return new PageBean(name, gender, begin, end, p.getTotal(), p.getResult());
            }

        }
    }

    /**
     * 根据id批量删除员工
     *
     * @param ids id列表
     */
    @Override
    public void deleteByIds(List<Integer> ids) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            empMapper.deleteByIds(ids);
        }
    }

    /**
     * 根据部门号删除员工<br>
     * 如果部门被删除而部门下的员工没有被删除,就会破坏数据的完整性和一致性
     *
     * @param deptId 部门号
     */
    @Override
    public void deleteByDeptId(Integer deptId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            empMapper.deleteByDeptId(deptId);
        }
    }

    /**
     * 添加员工
     *
     * @param emp Emp
     */
    @Override
    public void insert(Emp emp) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            emp.setCreateTime(LocalDateTime.now());
            emp.setUpdateTime(LocalDateTime.now());
            empMapper.insert(emp);
        }
    }

    /**
     * 根据id查询员工<hr>
     * 更新员工分为两步: <br>
     * 1.查询回显<br>
     * 2.提交更新
     *
     * @param id ID
     */
    @Override
    public Emp selectById(Integer id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            return empMapper.selectById(id);
        }
    }

    /**
     * 更新员工分为两步:<br>
     * 1.查询回显<br>
     * 2.提交更新
     *
     * @param emp Emp
     */
    @Override
    public void update(Emp emp) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            emp.setUpdateTime(LocalDateTime.now());
            empMapper.update(emp);
        }
    }

    /**
     * 根据用户名和密码查询
     */
    @Override
    public Emp selectByUsernameAndPassword(Emp emp) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
            return empMapper.selectByUsernameAndPassword(emp);
        }
    }
}
