package com.itheima.tliaswebmanagement.service.impl;

import com.itheima.tliaswebmanagement.mapper.DeptLogMapper;
import com.itheima.tliaswebmanagement.pojo.DeptLog;
import com.itheima.tliaswebmanagement.service.DeptLogService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeptLogServiceImpl implements DeptLogService {
    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public DeptLogServiceImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public void insert(DeptLog deptLog) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            DeptLogMapper deptLogMapper = sqlSession.getMapper(DeptLogMapper.class);
            deptLogMapper.insert(deptLog);
        }
    }
}
