package com.itheima.tliaswebmanagement.service;

import com.itheima.tliaswebmanagement.pojo.Dept;

import java.util.List;

public interface DeptService {

    /**
     * 查询所有部门数据
     *
     * @return List集合
     */
    List<Dept> list();

    /**
     * 根据id查询部门
     *
     * @param id id号
     * @return Dept对象
     */
    Dept selectById(Integer id);

    /**
     * 根据id删除部门
     *
     * @param id id号
     */
    void delete(Integer id) throws Exception;

    /**
     * 添加部门
     *
     * @param dept 部门对象
     */
    void insert(Dept dept);

    /**
     * 更新部门信息
     *
     * @param dept Dept对象
     */
    void update(Dept dept);
}
