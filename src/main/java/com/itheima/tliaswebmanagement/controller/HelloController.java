package com.itheima.tliaswebmanagement.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

/**
 * @author 高翔宇
 * @since 2023/12/26 周二 17:42
 */
@Controller
@Slf4j
public class HelloController {
    @RequestMapping("/")
    public void index(HttpServletResponse response) throws IOException {
        log.info("哈喽~");
        response.sendRedirect("http://localhost:90");
    }
}
