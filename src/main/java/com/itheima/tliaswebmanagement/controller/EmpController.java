package com.itheima.tliaswebmanagement.controller;

import com.itheima.tliaswebmanagement.anno.RecordOperationLog;
import com.itheima.tliaswebmanagement.pojo.Emp;
import com.itheima.tliaswebmanagement.pojo.PageBean;
import com.itheima.tliaswebmanagement.pojo.Result;
import com.itheima.tliaswebmanagement.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@RequestMapping("/emps")
@RestController
@Slf4j
public class EmpController {
    private final EmpService empService;

    public EmpController(EmpService empService) {
        this.empService = empService;
    }

    /**
     * 分页查询员工<br>
     * 前端将查询条件以key=value的形式传递过来<br>
     * 注解@RequestParam参数用于处理HTTP请求，将请求中的参数映射为方法参数。defaultValue可设置默认值
     *
     * <p style="color: #FF7F00; font-weight: bold; font-style: italic;">
     * &#064;RequestParam注解用于将请求参数绑定到Spring  MVC中的控制器方法参数上，
     * 如果该参数没有传递，则会在调用控制器方法时抛出异常。
     * 因此，添加了@RequestParam注解的参数在请求中是必须传递的。
     * </p>
     *
     * <p>
     * 当我们尝试将这个SpringBoot项目改为SpringMVC项目后，在此处出现了一个问题：起那段前端传递的时期无法被格式化为LocalDate等对象格式，
     * 会出现类型转换错误。
     * 求解无果后，我们只好使用以下策略：使用String接受日期参数；借助{@link SimpleDateFormat}解析日期并转化为为{@link LocalDate}对象。
     * </p>
     *
     * @param page     分页查询的页码
     * @param pageSize 分页查询的每页记录数
     * @return Result
     */
    @GetMapping
    public Result pageList(String name,
                           Short gender,
//                           @DateTimeFormat(pattern = "yyyy-MM-dd")
                           String begin,
//                           @DateTimeFormat(pattern = "yyyy-MM-dd")
                           String end,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer pageSize) throws ParseException {
        log.info("分页条件查询员工:name={},gender={},begin={},end={},page={},pageSize={}", name, gender, begin, end, page, pageSize);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" yyyy-MM-dd");

        LocalDate begin1 = begin != null && !begin.isEmpty() ? LocalDate.ofInstant(simpleDateFormat.parse(begin).toInstant(), ZoneId.systemDefault()) : null;
        LocalDate end1 = begin != null && !begin.isEmpty() ? LocalDate.ofInstant(simpleDateFormat.parse(end).toInstant(), ZoneId.systemDefault()) : null;
        PageBean pageBean = empService.pageList(
                name,
                gender,
                begin1,
                end1,
                page,
                pageSize);
        return Result.success(pageBean);
    }

    /**
     * 根据id批量删除员工
     *
     * @param ids id列表
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @RecordOperationLog
    public Result deleteByIds(@PathVariable List<Integer> ids) {
        log.info("根据id批量删除员工:{}", ids);
        empService.deleteByIds(ids);
        return Result.success();
    }

    /**
     * 新增员工
     *
     * @param emp Emp
     * @return Result
     */
    @PostMapping
    @RecordOperationLog
    public Result insert(@RequestBody @NonNull Emp emp) {
        log.info("新增员工:{}", emp);
        empService.insert(emp);
        return Result.success();
    }

    /**
     * 根据id查询员工<hr>
     * &#064;PathVariable：定义路径可变形参。将请求参数中的信息映射至Java函数的形参
     *
     * @param id ID
     * @return Result
     */
    @GetMapping("/{id}")
    public Result selectById(@PathVariable Integer id) {
        log.info("根据id查询员工:{}", id);
        return Result.success(empService.selectById(id));
    }

    /**
     * 更新员工分为两步:<br>
     * 1.查询回显<br>
     * 2.提交更新<hr>
     * &#064;RequestBody注解可以用来接收HTTP请求中的JSON格式的数据，并将其映射为指定的Java对象。
     *
     * @param emp Emp
     * @return Result
     */
    @PutMapping
    @RecordOperationLog
    public Result update(@RequestBody Emp emp) {
        log.info("更新员工:{}", empService.selectById(emp.getId()));
        empService.update(emp);
        return Result.success();
    }
}
