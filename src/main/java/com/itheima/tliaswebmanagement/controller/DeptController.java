package com.itheima.tliaswebmanagement.controller;

import com.itheima.tliaswebmanagement.anno.RecordOperationLog;
import com.itheima.tliaswebmanagement.pojo.Dept;
import com.itheima.tliaswebmanagement.pojo.Result;
import com.itheima.tliaswebmanagement.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

/**
 * &#064;RestController  = &#064;Controller + &#064;ResponseBody
 * <p>
 * &#064;ResponseBody注解用于将Spring MVC中的控制器方法返回的数据转换为HTTP响应主体。
 * </p>
 */
@Slf4j
@RequestMapping("/depts")//抽取路径的公共部分"/depts"
@RestController()
public class DeptController {
    private final DeptService deptService;

    @Autowired
    public DeptController(DeptService deptService) {
        this.deptService = deptService;
    }

    /**
     * 查询部门列表数据
     * <p>
     * 在@RequestMapping注解中需要指定访问方式为GET，因为这是需求文档的要求。
     * </p>
     * <p>
     * 当然，我们还有其他的方式限定请求方式，比如将@RequestMapping换成它的衍生注解@GetMapping
     * </p>
     *
     * @return Result对象, 它封装了数据
     */
//    @RequestMapping(value = "/depts", method = RequestMethod.GET)
    @GetMapping
    public Result list() {
        log.info("查询部门...");
        System.out.println("爱你");
        /*
         * 当我们在Controller层的方法上添加@ResponseBody注解时，
         * Spring框架会自动将方法的返回值转换为HTTP响应体，并返回给客户端。
         * @RestController已包含了@ResponseBody注解。
         * */
        return Result.success(deptService.list());
    }

    /**
     * 根据id查询部门
     *
     * @return Result
     */
    @GetMapping("/{id}")
    public Result selectById(@PathVariable Integer id) {
        log.info("根据id查询部门~（id%s）{}", id);
        return Result.success(deptService.selectById(id));
    }

    /**
     * 根据id删除部门
     *
     * @param id id号
     * @return Result
     */
    @DeleteMapping("/{id}")
    @RecordOperationLog
    public Result delete(@PathVariable Integer id) throws Exception {
        System.out.printf("根据id删除部门:%s%n", id);
        deptService.delete(id);
        return Result.success();
    }

    /**
     * 新增部门<br>
     * 为参数添加@RequestBody注解可以将请求的JSON数据映射为对象并传递给处理程序方法。
     *
     * @param dept 部门对象
     * @return Result对象
     */
    @PostMapping
    @RecordOperationLog
    public Result add(@RequestBody @NonNull Dept dept) {
        System.out.printf("新增部门:%s%n", dept.getName());
        deptService.insert(dept);
        return Result.success();
    }

    /**
     * 更新部门信息
     *
     * @return Result
     */
    @PutMapping
    @RecordOperationLog
    public Result update(@RequestBody Dept dept) {
        System.out.printf("更新部门信息:%s%n", dept.getName());
        deptService.update(dept);
        return Result.success();
    }
}
