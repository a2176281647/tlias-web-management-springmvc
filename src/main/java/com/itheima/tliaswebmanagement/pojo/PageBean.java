package com.itheima.tliaswebmanagement.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageBean {
    private String name;
    private Short gender;
    private LocalDate begin;
    private LocalDate end;
    private Long total;// 总记录数
    private List rows;// 每页数量
}
