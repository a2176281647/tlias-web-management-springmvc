package com.itheima.tliaswebmanagement.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data// get和set方法、toString方法、equals和hashCode方法
@NoArgsConstructor// 无参构造
@AllArgsConstructor// 全参构造
@Builder
public class Emp {
    private int id;// id
    private String username;// 用户名
    private String password;// 密码
    private String name;// 姓名
    private short gender;// 性别
    private String image;// 图片
    private String job;// 职位
    private LocalDate entrydate;// 入职日期
    private int deptId;// 部门id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;// 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;// 更新时间
}
