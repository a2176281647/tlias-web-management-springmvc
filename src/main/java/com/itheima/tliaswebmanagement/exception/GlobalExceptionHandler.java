package com.itheima.tliaswebmanagement.exception;

import com.itheima.tliaswebmanagement.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;

/**
 * 全局异常处理器
 * <p>
 * &#064;RestControllerAdvice是Spring框架中的一种标示全局异常处理类的注解。它用于捕获Spring  Boot应用程序中发生的全局异常，并生成相应的响应。
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)//指定异常类型
    public Result ex(Exception e) {
        log.error(e.getMessage());
        System.out.println(e.getClass());
        Arrays.stream(e.getStackTrace()).forEach(System.out::println);
        return Result.error("你在搞什么呀？");// 前段会把msg信息渲染展示
    }
}
