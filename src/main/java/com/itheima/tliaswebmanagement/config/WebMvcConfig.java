package com.itheima.tliaswebmanagement.config;

import com.itheima.tliaswebmanagement.interceptor.LoginCheckInterceptor;
import com.itheima.tliaswebmanagement.json.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Configuration
@Slf4j
public class WebMvcConfig extends WebMvcConfigurationSupport {
    private final LoginCheckInterceptor loginCheckInterceptor;

    @Autowired
    public WebMvcConfig(LoginCheckInterceptor loginCheckInterceptor) {
        this.loginCheckInterceptor = loginCheckInterceptor;
    }

    /**
     * 注册拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("注册拦截器...");
        registry.addInterceptor(loginCheckInterceptor)
                .addPathPatterns("/**")// 需要两个小星星哦
                .excludePathPatterns("/login", "/static/**");// 不要拦截/login和静态资源哦~
    }

    /**
     * 拓展消息转换器
     */
    @Override
    public void extendMessageConverters(@NonNull List<HttpMessageConverter<?>> converters) {
        log.info("拓展消息转换器...");
        converters.add(new MappingJackson2HttpMessageConverter(new JacksonObjectMapper()));
    }
}