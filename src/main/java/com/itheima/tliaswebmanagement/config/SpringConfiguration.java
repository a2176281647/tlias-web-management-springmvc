package com.itheima.tliaswebmanagement.config;

import com.itheima.tliaswebmanagement.utils.AliOSSProperties;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author 高翔宇
 * @since 2023/12/26 周二 10:10
 */
@Configuration
public class SpringConfiguration {
    /**
     * Mybatis-{@link SqlSessionFactory}
     */
    @Bean
    public SqlSessionFactory sqlSessionFactory() throws IOException {
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        return sqlSessionFactoryBuilder.build(Resources.getResourceAsStream("mybatis.xml"));
    }

    /**
     * 阿里云配置
     */
    @Bean
    public AliOSSProperties aliOSSProperties() {
        AliOSSProperties aliOSSProperties = new AliOSSProperties();
        aliOSSProperties.setAccessKeyId("LTAI5tSTnrNJSy6fH7XvteWi");
        aliOSSProperties.setSecretAccessKey("CdbXZN8ZWVIRvd2WYP5qxy1r40yzX8");
        aliOSSProperties.setEndpoint("https://oss-cn-hangzhou.aliyuncs.com");
        aliOSSProperties.setBucketName("web-tlias-falling-dust");
        return aliOSSProperties;
    }
}
