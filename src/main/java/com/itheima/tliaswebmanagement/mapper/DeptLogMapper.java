package com.itheima.tliaswebmanagement.mapper;

import com.itheima.tliaswebmanagement.pojo.DeptLog;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 记录日志
 */
@Mapper
public interface DeptLogMapper {

    @Insert("insert into springboot_web_mybatis_project.dept_log(create_time,description) values(#{createTime},#{description})")
    void insert(DeptLog log);

}
