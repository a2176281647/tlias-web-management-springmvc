package com.itheima.tliaswebmanagement.mapper;

import com.itheima.tliaswebmanagement.pojo.Dept;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Mapper接口不需要实现类,因为实现了@Mapper注解后会自动映射实现类对象
 */
@Mapper
public interface DeptMapper {
    /**
     * 查询所有部门
     *
     * @return Result
     */
    @Select("select * from springboot_web_mybatis_project.dept")
    List<Dept> list();

    /**
     * 根据id查询部门
     *
     * @param id id号
     * @return Dept对象
     */
    @Select("select * from springboot_web_mybatis_project.dept where id=#{id}")
    Dept selectById(Integer id);

    /**
     * 根据id删除部门
     *
     * @param id id号
     */
    @Delete("delete from springboot_web_mybatis_project.dept where id = #{id}")
    void delete(Integer id);

    /**
     * 新增部门
     *
     * @param dept Dept对象
     */
    @Insert("insert into springboot_web_mybatis_project.dept (name, create_time, update_time) values (#{name},#{createTime},#{updateTime})")
    void insert(Dept dept);

    /**
     * 更新部门信息
     *
     * @param dept Dept对象
     */
    @Update("update springboot_web_mybatis_project.dept set name=#{name},update_time=#{updateTime} where id=#{id}")
    void update(Dept dept);
}
