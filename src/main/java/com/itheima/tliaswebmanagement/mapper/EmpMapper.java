package com.itheima.tliaswebmanagement.mapper;

import com.itheima.tliaswebmanagement.pojo.Emp;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Mapper接口不需要实现类,因为实现了@Mapper注解后会自动映射实现类对象
 */
@Mapper
public interface EmpMapper {
    /**
     * 放弃前两个方法,使用PageHelper依赖实现分页查询<br>
     * 使用xml配置文件映射SQL语句
     *
     * @return List<Emp>
     */
    List<Emp> pageList(@Param("name") String name, @Param("gender") Short gender, @Param("begin") LocalDate begin, @Param("end") LocalDate end, @Param("page") Integer page, @Param("pageSize") Integer pageSize);

    /**
     * 根据id批量删除员工
     *
     * @param ids id列表
     */
    void deleteByIds(@Param("ids") List<Integer> ids);

    /**
     * 根据部门号删除员工<br>
     * 如果部门被删除而部门下的员工没有被删除,就会破坏数据的完整性和一致性
     */
    @Delete("delete from emp where dept_id=#{deptId}")
    void deleteByDeptId(Integer deptId);

    /**
     * 新增员工
     *
     * @param emp Emp
     */
    @Insert("insert into emp " +
            "(username, name, gender, image, job, entrydate, dept_id, create_time, update_time) values " +
            "(#{username}, #{name}, #{gender}, #{image}, #{job}, #{entrydate}, #{deptId}, #{createTime}, #{updateTime})")
    void insert(Emp emp);

    /**
     * 根据id查询员工
     *
     * @param id ID
     * @return Emp
     */
    @Select("select * from emp where id=#{id}")
    Emp selectById(Integer id);

    /**
     * 更新员工信息
     *
     * @param emp Emp
     */
    void update(Emp emp);

    /**
     * 根据用户名和密码查询员工
     *
     * @param emp Emp
     */
    @Select("select * from emp where username=#{username} and password=#{password}")
    Emp selectByUsernameAndPassword(Emp emp);
}
