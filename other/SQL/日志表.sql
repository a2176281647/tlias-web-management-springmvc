create database if not exists springboot_web_mybatis_project;

use springboot_web_mybatis_project;

-- 操作日志表
create table operate_log
(
    id            int unsigned primary key auto_increment comment 'ID',
    operate_user  int unsigned comment '操作人ID',
    operate_time  datetime comment '操作时间',
    class_name    varchar(100) comment '操作的类名',
    method_name   varchar(100) comment '操作的方法名',
    method_params varchar(1000) comment '方法参数',
    return_value  varchar(2000) comment '返回值',
    cost_time     bigint comment '方法执行耗时, 单位:ms'
) comment '操作日志表';

create table dept_log
(
    id          int auto_increment comment '主键ID' primary key,
    create_time datetime     null comment '操作时间',
    description varchar(300) null comment '操作描述'
) comment '部门操作日志表';